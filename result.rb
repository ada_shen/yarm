# frozen_string_literal: true

# YARM: Yet Another Ruby Monad

# A Result is a Monad that is either Ok or an Error. Used as an alternative to exception handling.
class Yarm::Result < Yarm::Union
	# For now, not writing an actual `Monad` mixin as I don't think there's actually much value for it in Ruby?
	# I don't plan to implement "do notation" for now as I'm unsatisfied with the possibilities for doing so in Ruby.

	Ok(value: Yarm::Unit) do
		def error
			raise UnexpectedOk.new(value)
		end

		def deconstruct
			value == Yarm::Unit ? [] : [value]
		end
	end

	Error(:error) do
		def value
			raise UnexpectedError.new(error)
		end
	end

	# Wraps a value in Result::Ok if it is not already a Result
	def self.wrap(value)
		value.is_a?(Yarm::Result) ? value : Ok(value)
	end

	# Wraps a block to return a Result.
	# Returns OK if the block returns successfully.
	# By default, rescues any StandardError and wraps it in Error.
	#
	# @param rescuing [#===] The error class or other pattern to rescue
	# @param as [Object] If provided, this value will be used as the Error value instead of the rescued exception
	#
	# @return [Yarm::Result]
	def self.try(rescuing: StandardError, as: nil)
		Ok(yield)
	rescue rescuing => err
		Error(as || err)
	end

	# Converts an array of Results to a single Result containing an array
	#
	# @param results [Array<Yarm::Result>] The array of Results to convert
	#
	# @return [Yarm::Result<Array>]
	#     If all the Results were Ok, then an array of their values.
	#     If any Result was an Error, then an array of the error values.
	def self.all(results)
		(oks, errors) = results.partition(&:ok?)
		errors.empty? ? Ok(oks.map(&:value)) : Error(errors.map(&:error))
	end

	# Flattens Ok(Result) to the inner Result
	#
	# @param depth [Integer] The depth to which to flatten. Defaults to nil, meaning infinite depth.
	#
	# @return [Yarm::Result]
	def flatten(depth = nil)
		return self if depth&.zero?

		case self
			in Ok(Yarm::Result => inner) then inner.flatten(depth &.- 1)
			in not_nested then not_nested
		end
	end

	# Composes this Result with another Result-returning operation
	#
	# @yieldparam value [Object] The value of this Result, if Ok
	# @yieldreturn [Yarm::Result] A new Result derived from the first
	#
	# @return [Yarm::Result] If this Result is Ok, the result of the block. If this Result is an Error, this Result.
	def bind
		case self
			in Ok(value)
				(yield value).tap do |new_result|
					unless new_result.is_a?(Yarm::Result)
						raise <<~ERR.gsub(/\s+/, ' ')
							The block given to Result#bind must return a new Result.
							Use Result#map or Result#and_then to return an unwrapped value.
						ERR
					end
				end
			in Error => err then err
		end
	end

	# Combines #bind with the rescuing behavior of Result.try
	def try_bind(rescuing: StandardError, as: nil)
		bind do
			yield
		rescue rescuing => err
			Error(as || err)
		end
	end

	# Maps the value of this Result through a function
	#
	# @return [Yarm::Result]
	#     If this Result is Ok, a new Result with the mapping applied to the value.
	#     If this Result is an Error, this Result.
	def map
		case self
			in Ok(value) then Ok(yield value)
			in Error => err then err
		end
	end

	# Combines #map with the rescuing behavior of Result.try
	def try_map(rescuing: StandardError, as: nil, &block)
		map(&block)
	rescue rescuing => err
		Error(as || err)
	end

	# Combines #bind with #map. If the block returns a non-Result, it is wrapped.
	def and_then
		bind { Yarm::Result.wrap(yield _1) }
	end

	# Combines #and_then with the rescuing behavior of Result.try
	def and_then_try(rescuing: StandardError, as: nil)
		try_bind(rescuing: rescuing, as: as) { Yarm::Result.wrap(yield _1) }
	end

	# Like #map, but maps the Error value rather than the Ok value
	def map_error
		case self
			in Ok => ok then ok
			in Error(err) then Error(yield err)
		end
	end

	# Like #bind, but binds the Error value rather than the Ok value
	def bind_error
		case self
			in Ok => ok then ok
			in Error(err)
				(yield err).tap do |new_result|
					unless new_result.is_a?(Yarm::Result)
						raise <<~ERR.gsub(/\s+/, ' ')
							The block given to Result#bind_error must return a new Result.
							Use Result#map_error or Result#handle_error to return an unwrapped value.
						ERR
					end
				end
		end
	end

	# Like #and_then, but maps the Error value rather than the Ok value
	def handle_error
		bind_error { Yarm::Result.wrap(yield _1) }
	end

	# Filters out Result values.
	# If this Result is Ok, then the value is passed to the block, which can reject and convert it to an Error.
	#
	# @yieldparam value [Object] The Ok value of this Result
	# @yieldreturn [Object]
	#     If nil or false, the Ok value is kept.
	#     If true, then the result will be Error(:rejected)
	#     If any other value, then that value will be wrapped in Error as the result of the method.
	#
	# @return [Yarm::Result] A Result consistent with the state of this Result and the result of the block
	#
	# @example
	#     result.reject { :not_acceptable unless _1.acceptable? } # => Error(:not_acceptable) or Ok(value)
	def reject
		case self
			in Ok(value)
				rejection = yield value
				case rejection
					when nil, false then Ok(value)
					when true then Error(:rejected)
					else Error(rejection)
				end
			in Error => error then error
		end
	end

	# Yields the value if this Result is Ok. Returns self.
	def tap_ok
		yield value if ok?
		self
	end

	# Yields the error if this Result is an Error. Returns self.
	def tap_error
		yield error if error?
		self
	end

	# Raises an exception if this Result is not Ok
	def assert_ok!
		raise UnexpectedError.new(error) if error?
	end

	# Returns true if this Result is Ok, containing a value matching the given pattern via ===
	def contain?(pattern)
		case self
			in Ok(value) then patern === value
			in Error then false
		end
	end

	# If this Result is Ok, returns its value,
	# otherwise returns either the value passed to the method or the result of the block given
	def or(*args)
		raise "Cannot pass both an argument and a block to Result#or" if !args.empty? && block_given?
		case self
			in Ok(value) then value
			in Error then args.empty? ? yield : args[0]
		end
	end

	# Appends another Result to this one
	#
	# @param other [Yarm::Result] The Result to append to this one
	# @param append_value [#call] Called if both Results are Ok to append the two result values
	# @param append_error [#call] Called if both Results are Errors to append the two error values
	def append(other, append_value, append_error)
		case [self, other]
			in [Ok(a), Ok(b)] then Ok(append_value.(a, b))
			in [Error => error, Ok] then error
			in [Ok, Error => error] then error
			in [Error(err_a), Error(err_b)] then append_error.(err_a, err_b)
		end
	end

	# Raised by Result::Ok#error
	class UnexpectedOk < StandardError
		attr_accessor :value

		def initialize(result)
			super("Expected Result::Error. Instead got Ok(#{value.inspect})")
			@value = value
		end
	end

	# Raised by Result::Error#value
	class UnexpectedError < StandardError
		attr_accessor :error

		def initialize(error)
			super(error.inspect)
			@error = error
		end
	end
end
