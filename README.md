# YARM: Yet Another Ruby Monad

A pure-Ruby implementation of ergonomic discriminated unions/ADTs, as well as a Result monad.

Not extensively tested at the moment. Maybe one day I'll figure out how Rubygems works and publish this.

Prior art:
* [dry-monads](https://dry-rb.org/gems/dry-monads) -
  Another monad implementation. Gave me the idea of generating mixins for unions.
* [literal_enums](https://github.com/joeldrapper/literal_enums) -
  Major source of inspiration for the literal DSL for `Union`.
  Provides a similar DSL for defining C-style value-based enums.
  May be a more appropriate choice if that's what you're looking for.
  Could also make a good companion to YARM if you want both enums and ADTs.

## `Union`

Defines a discriminated union/ADT:
```rb
class MyUnion < Yarm::Union
    # Define union cases simply by listing them.
    # Providing no arguments creates a singleton object.
    # Defines MyUnion::SingletonCaseClass with MyUnion::SingletonCase as its sole instance.
    SingletonCase

    # Provide an argument list with symbols to define pattern-matchable properties.
    # Defines MyUnion::CaseWithArgs as a class.
    # Aliases .new as both .call and .[].
    # Instances can be constructed with `CaseWithArgs(42)`. `CaseWithArgs(value: 42)` is also valid.
    CaseWithArgs(:value)

    # Kwargs can be used to mark arguments as optional. A default can either be a static value or a lazy callable.
    CaseWithDefaultArgs(value: nil, foo: -> { lazy_default })

    # A block can be provided. It will be executed in the context of the case class.
    ExtendedCase do
        def to_s
            'Custom to_s for ExtendedCase'
        end
    end

    # Instance methods can be defined in the main class and all cases will inherit them
    def shared_method
        42
    end

    # Methods can be annotated with `matchable` to include them in #deconstruct_keys
    matchable def singleton
        self in SingletonCase | ExtendedCase
    end

    # Each case has predicate methods for all of the cases.
    # If String responds to #snakecase in your environment, that is used, otherwise a built-in implementation.
    matchable def singleton_alt
        singleton_case? || extended_case?
    end

    # If the method returns a fixed value, you can use kwargs as a shorthand.
    # This can be useful to make a property matchable across all the cases which lack it.
    matchable(foo: 'hi') # Equivalent to `matchable def foo; 'hi'; end`
end

class MyClass
    # Each union generates a Mixin module that allows using the case names unprefixed
    include MyUnion::Mixin

    def match_union(my_union)
        case my_union
            in SingletonCase then :singleton
            in CaseWithArgs(21) then :twenty_one
            in CaseWithDefaultArgs(foo: ^lazy_default) then :default_foo
            in _ then :other
        end
    end
end
```

## `Result`

A Result monad with cases `Ok` and `Error`. Very loosely based on the F# `Result` module.

```rb
# Basic usage example. See the source code for more details.
def find_record(id)
    Yarm::Result.try(rescuing: RecordNotFound, as: :not_found) do
        Record.find(id)
    end
end

def validation_errors(record)
    return :no_a unless record.a
    return :no_b unless record.b
    return :no_c unless record.c
end

def process_record(record)
    Yarm::Result.try do
        # Business logic...
    end
end

def execute
    result =
        find_record(record_id)
            .reject { validation_errors(_1) }
            .and_then { process_record(_1) }

    case result
        in Ok(value) then value
        in Error(:not_found) then create_record
        in Error((:no_a | :no_b | :no_c) => validation_error) then render_validation_error(validation_error)
        in Error(err) then log(err)
    end
end
```

There is also `Yarm::Unit`, which can be thought of as a "void" value.
It is the default value for `Ok`. `Ok(Yarm::Unit)#deconstruct` returns an empty array,
meaning that a value constructed as `Ok()` pattern matches as `Ok()` and is distinct from `Ok(nil)`.
