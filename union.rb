# frozen_string_literal: true

# YARM: Yet Another Ruby Monad

# Base class for effortlessly definining pattern-matchable, immutable, ergonomic discriminated unions in Ruby.
class Yarm::Union
	class << self
		def abstract_union_class?
			true
		end

		def inherited(subclass)
			# When the subclass is done initializing (can't believe we can hook into that),
			# set an instance variable that forbids defining new cases for the union.
			TracePoint.new(:end) do |tp|
				if tp.self == subclass
					tp.disable
					subclass.instance_variable_set(:@sealed, true)
					subclass.finalize_cases

					# The inherited method is not... inherited. So we need to do this.
					class << subclass
						def inherited(_subclass)
							raise "Cannot extend sealed union class #{self.name}"
						end
					end
				end
			end.enable
		end

		# An array of the case classes defined for this Union
		#
		# @return [Array<Class>]
		def cases
			@cases ||= []
		end

		def const_missing(name)
			return super if !abstract_union_class? || @sealed
			define_case(name)
		end

		def method_missing(name, *args, **kwargs, &block)
			return super if !abstract_union_class? || @sealed || !/[[:upper:]]/.match?(name.to_s[0])

			props = {}

			args.each do |prop|
				raise "Union property names must be Symbols, got #{prop.inspect}" unless prop.is_a?(Symbol)
				props[prop] = nil
			end

			kwargs.each do |prop, default|
				raise "Union property names must be Symbols, got #{prop.inspect}" unless prop.is_a?(Symbol)
				props[prop] = default.respond_to?(:call) ? default : -> { default }
			end

			define_case(name, props, &block)
		end

		# Defines a new Union case.
		# Any block passed will be executed in the context of the newly defined class.
		#
		# @param name [Symbol] The name of the case to create
		# @param props [Hash<Symbol => #call>]
		private def define_case(name, props = {}, &block)
			snake_name = Yarm::Union.snakecase(name.to_s)
			is_singleton = props.empty?

			case_class = Class.new(self) do
				extend CaseClassMethods

				@props = props.freeze

				define_method :"#{snake_name}?" do
					true
				end

				protected def matchable_values
					props = self.class.reflect_on_props.keys
					matchable_methods = self.class.send(:matchable_methods)
					shared_matchable_methods = self.class.superclass.send(:matchable_methods)
					(props.each + matchable_methods.each + shared_matchable_methods.each).map { [_1, send(_1)] }
				end

				def deconstruct_keys(keys)
					# Always include the type name so that singletons don't match empty Hashes
					result = { _type: snake_name.to_sym }

					matchable_values.each do |key, value|
						result[key] = value if keys == nil || keys.include?(key)
					end

					result
				end

				if is_singleton
					def self.singleton?
						true
					end

					def singleton?
						true
					end
				else
					attr_reader *props.keys

					class << self
						alias_method :call, :new
						alias_method :[], :new

						def singleton?
							false
						end
					end

					def singleton?
						false
					end

					def deconstruct
						self.class.reflect_on_props.keys.map { send(_1) }
					end

					def ==(other)
						return false unless other.is_a?(self.class)

						self.matchable_values == other.matchable_values
					end
				end

				class_exec(&block) if block
			end

			cases << case_class

			if is_singleton
				const_set(:"#{name}Class", case_class)
				instance = case_class.new
				const_set(name, instance)
				case_class.const_set(:Instance, instance)
			else
				const_set(name, case_class)
			end
		end

		# Called after all Union cases have been defined to finalize the classes and define name-dependent methods
		protected def finalize_cases
			# Pull out of self for use in the module block
			cases = self.cases
			cases.freeze

			cases.each do |case_class|
				short_name = case_class.name.chomp('Class').split('::').last

				(cases - [case_class]).each do |other_case|
					other_short_name = other_case.name.chomp('Class').split('::').last
					case_class.define_method :"#{Yarm::Union.snakecase(other_short_name)}?" do
						false
					end
				end

				# Define the same methods that we do in the mixin below, but public on the class itself
				define_method short_name do |*args, **kwargs|
					if case_class.singleton?
						unless args.empty? && kwargs.empty?
							raise ArgumentError, <<~ERR.gsub(/\s+/, ' ')
								#{short_name} is a singleton case.
								Expected no arguments, received #{args.length} args and #{kwargs.size} kwargs.
							ERR
						end

						case_class::Instance
					else
						case_class.new(*args, **kwargs)
					end
				end
			end

			const_set(:Mixin, Module.new do
				cases.each do |case_class|
					short_name = case_class.name.chomp('Class').split('::').last

					# I'm honestly kind of surprised this works, but you can in fact have a constant and a method
					# with the same name and Ruby just kind of... knows which to use?
					# It behaves perfectly as far as I can tell.
					# MyCase(arg) calls the method and MyCase on its own references the constant.
					# In the case of singletons they both return the same thing so it doesn't matter.

					const_set(short_name, case_class.singleton? ? case_class::Instance : case_class)
					private_constant short_name

					define_method short_name do |*args, **kwargs|
						if case_class.singleton?
							unless args.empty? && kwargs.empty?
								raise ArgumentError, <<~ERR.gsub(/\s+/, ' ')
									#{short_name} is a singleton case.
									Expected no arguments, received #{args.length} args and #{kwargs.size} kwargs.
								ERR
							end

							case_class::Instance
						else
							case_class.new(*args, **kwargs)
						end
					end
					private short_name
				end
			end)
		end

		def snakecase(string)
			if string.respond_to?(:snakecase)
				string.snakecase
			else
				string.gsub(/(?<=[a-z0-9])[A-Z]+|(?<=[a-zA-Z])[0-9]/) { "_#{_1.downcase}" }.downcase
			end
		end
	end

	# Defines class methods for marking instance methods as pattern-matchable
	module MatchableMethods
		# A Set of method names marked as pattern-matchable
		#
		# @return [Set<Symbol>]
		private def matchable_methods
			@matchable_methods ||= Set.new
		end

		# Marks one or more methods as matchable.
		# Can also define methods returning constant values via kwargs.
		#
		# @param names [Array<Symbol>] The names of existing methods to mark as matchable
		# @param defintions [Hash<Symbol => Object>] Mapping from method names to the constant values they should return
		private def matchable(*names, **definitions)
			definitions.each do |name, value|
				define_method name do
					value
				end
			end

			names += definitions.keys

			matchable_methods.merge(names)

			# Allow chaining with `private` and other modifiers
			names.length == 1 ? names.first : names
		end
	end
	private_constant :MatchableMethods

	extend MatchableMethods

	module CaseClassMethods
		include MatchableMethods

		def abstract_union_class?
			false
		end

		def inherited(_subclass)
			raise "Cannot extend union case #{self.name}"
		end

		# Returns an array of the property names defined for this case class
		#
		# @return [Array<Symbol>]
		def reflect_on_props
			@props
		end
	end
	private_constant :CaseClassMethods

	def initialize(*args, **kwargs)
		if self.class.try(:abstract_union_class?)
			raise "Cannot directly instantiate abstract union class #{self.class.name}"
		end

		props = self.class.reflect_on_props

		# Handle singleton initialization
		if props.empty?
			if defined?(@@initialized) && @@initialized
				raise "Cannot instantiate singleton union case #{self.class.name}"
			else
				@@initialized = true
			end
		end

		props.each_with_index do |(prop, default), i|
			# Props consume positional arguments eagerly
			if args.present?
				value = args.shift
			elsif kwargs.key?(prop)
				value = kwargs[prop]
			else
				if default
					value = default.()
				else
					raise "Required kwarg #{prop} not provided"
				end
			end

			instance_variable_set(:"@#{prop}", value)
		end

		freeze
	end

	def inspect(multiline: false)
		result = self.class.name.chomp('Class')

		props = self.class.reflect_on_props
		if props.present?
			result = result.dup + '('
			result << "\n" if multiline
			props.each_with_index do |(prop, default), i|
				prop_str = send(prop).inspect
				prop_str = prop_str.gsub(/^/, '  ') if multiline
				result << prop_str
				result << ',' unless i == props.size - 1
				result << "\n" if multiline
			end
			result << ')'
		end

		result
	end

	alias to_s inspect
end
