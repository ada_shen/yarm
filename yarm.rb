# frozen_string_literal: true

# YARM: Yet Another Ruby Monad
module Yarm
	VERSION = '0.2.0'

	# Singleton class for Yarm::Unit
	class UnitClass
		private_class_method :new

		def self.instance
			@instance ||= self.new
		end

		def unit?
			true
		end
	end

	# A "unit" value that can be used to represent a meaningless non-value. Can be thought of as a "void" type.
	Unit = UnitClass.instance
end
